/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   easyfind.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/21 20:22:00 by elopez-r          #+#    #+#             */
/*   Updated: 2021/07/22 20:38:57 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef EASYFIND_HPP
#define EASYFIND_HPP

#include <exception>
#include <algorithm>

using std::find;

class NotFoundException: public std::exception
{
	public:
		const char* what() const throw()
		{
			return ("Can't find integer");
		}
};

/*
 * This function receives a container of integers and a integer to search,
 * returning an iterator to the found integer in the container or throwing
 * a NotFoundException if it can't be found.
 */
template <typename T>
typename T::iterator
easyfind(T& container, int n)
{
	typename T::iterator it;

	it = find(container.begin(), container.end(), n);
	if (it == container.end())
		throw NotFoundException();
	return (it);
}

#endif
