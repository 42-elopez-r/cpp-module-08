/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/22 12:30:05 by elopez-r          #+#    #+#             */
/*   Updated: 2021/07/23 19:20:29 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "easyfind.hpp"
#include <iostream>
#include <vector>

using std::cout; using std::endl;
using std::vector;

static void
displayVector(vector<int>& v)
{
	cout << "Vector: {";
	for (unsigned int i = 0; i < v.size(); i++)
		cout << " " << v[i];
	cout << " }" << endl;
}

int
main()
{
	vector<int> v(5);
	vector<int>::iterator it;

	for (unsigned int i = 0; i < v.size(); i++)
		v[i] = i;

	displayVector(v);
	cout << endl << "Use easyfind to search a 3:" << endl;

	try
	{
		it = easyfind<vector<int> >(v, 3);
		cout << "Found!" << endl;
	}
	catch (NotFoundException& e)
	{
		cout << e .what()<< endl;
		return (1);
	}

	cout << "Replace the 3 with a 42 in the vector using the iterator:" << endl;
	*it = 42;
	displayVector(v);

	cout << endl << "Use easyfind to search a 400:" << endl;
	try
	{
		easyfind<vector<int> >(v, 3);
		cout << "Found!" << endl;
	}
	catch (NotFoundException& e)
	{
		cout << e.what() << endl;
	}

	return (0);
}
