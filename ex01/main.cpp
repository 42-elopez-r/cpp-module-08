/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/23 12:18:51 by elopez-r          #+#    #+#             */
/*   Updated: 2021/07/23 14:35:35 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "span.hpp"
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <vector>
#include <algorithm>

using std::cout; using std::endl;
using std::vector; using std::generate;

static void
loadSpanWithRandom(Span& sp, size_t n)
{
	static bool random_seed_initialized = false;
	vector<int>* vec;

	if (!random_seed_initialized)
	{
		srand(time(NULL));
		random_seed_initialized = true;
	}

	vec = new vector<int>(n);
	generate(vec->begin(), vec->end(), rand);
	try
	{
		sp.addNumbers(vec->begin(), vec->end());
		cout << n << " random numbers successfully added" << endl;
	}
	catch (Span::NoMoreRoomSadNoisesException& e)
	{
		cout << "Can't add " << n << " random numbers to the Span object: ";
		cout << e.what() << endl;
	}
	delete vec;
}

int
main()
{
	Span sp1(5);
	Span* sp2;
	Span sp3(1);

	sp1.addNumber(5);
	sp1.addNumber(3);
	sp1.addNumber(17);
	sp1.addNumber(9);
	sp1.addNumber(11);

	cout << "Span sp1: ";
	sp1.display();
	cout << "Shortest span: " << sp1.shortestSpan() << endl;
	cout << "Longest span: " << sp1.longestSpan() << endl;

	cout << endl << "Generate Span sp2 with 100.000 numbers:" << endl;
	sp2 = new Span(100000);
	loadSpanWithRandom(*sp2, 100000);
	cout << "sp2 size: " << sp2->size() << endl;
	cout << "Shortest span: " << sp2->shortestSpan() << endl;
	cout << "Longest span: " << sp2->longestSpan() << endl;

	cout << endl << "Copying sp2 to sp3:" << endl;
	sp3 = *sp2;
	cout << "sp3 size: " << sp3.size() << endl;
	cout << "Shortest span: " << sp3.shortestSpan() << endl;
	cout << "Longest span: " << sp3.longestSpan() << endl;

	delete sp2;
}
