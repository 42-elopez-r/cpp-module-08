/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   span.hpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/22 20:33:56 by elopez-r          #+#    #+#             */
/*   Updated: 2021/07/23 14:13:18 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SPAN_HPP
#define SPAN_HPP

#include <exception>
#include <vector>

using std::vector;

class Span
{
	public:
		class NoMoreRoomSadNoisesException: public std::exception
		{
			public:
				const char* what() const throw();
		};
		class NotEnoughNumbersOMGException: public std::exception
		{
			public:
				const char* what() const throw();
		};

		Span(unsigned int max);
		Span(const Span& span);
		~Span();
		Span& operator=(const Span& span);
		void addNumber(int number);
		template <typename InputIterator>
		void addNumbers(InputIterator begin, InputIterator end);
		unsigned int shortestSpan() const;
		unsigned int longestSpan() const;
		void display() const;
		size_t size() const;
	
	private:
		vector<int>* _vec;
		unsigned int _max;

		void killEmAll();
		Span();
};

#include "span_addNumbers.hpp"

#endif
