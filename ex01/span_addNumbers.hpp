/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   span_addNumbers.hpp                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/23 14:11:41 by elopez-r          #+#    #+#             */
/*   Updated: 2021/07/23 14:17:42 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SPAN_ADDNUMBERS
#define SPAN_ADDNUMBERS

/*
 * This method adds a range of numbers to the Span object, throwing a
 * NoMoreRoomSadNoisesException if it wouldn't fit or if it's already full.
 */
template <typename InputIterator>
void
Span::addNumbers(InputIterator begin, InputIterator end)
{
	if ((_vec->size() + distance(begin, end)) > _max)
		throw NoMoreRoomSadNoisesException();
	_vec->insert(_vec->end(), begin, end);
}

#endif
