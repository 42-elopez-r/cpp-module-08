/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   span.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/22 20:48:10 by elopez-r          #+#    #+#             */
/*   Updated: 2021/07/23 14:12:59 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "span.hpp"
#include <algorithm>
#include <iostream>

using std::cout; using std::endl;
using std::copy; using std::sort;
using std::distance;

/*
 * This constructor sets the Span object to contain a maximum of max integers.
 */
Span::Span(unsigned int max)
{
	_max = max;
	_vec = new vector<int>;
	_vec->reserve(_max);
}

/*
 * Copy constructor.
 */
Span::Span(const Span& span)
{
	_max = 0;
	_vec = nullptr;
	*this = span;
}

/*
 * Destructor/
 */
Span::~Span()
{
	killEmAll();
}

/*
 * Assignation operator overload.
 */
Span&
Span::operator=(const Span& span)
{
	killEmAll();
	_max = span._max;
	_vec = new vector<int>(span._vec->size());
	_vec->reserve(_max);
	copy(span._vec->begin(), span._vec->end(), _vec->begin());

	return (*this);
}

/*
 * This method adds a number to the Span object, throwing a
 * NoMoreRoomSadNoisesException if it's already full.
 */
void
Span::addNumber(int number)
{
	if (_vec->size() == _max)
		throw NoMoreRoomSadNoisesException();
	_vec->push_back(number);
}

/*
 * This method returns the shortest span amongst the stored numbers.
 */
unsigned int
Span::shortestSpan() const
{
	unsigned int span;

	if (_vec->size() < 2)
		throw NotEnoughNumbersOMGException();

	sort(_vec->begin(), _vec->end());
	span = _vec->back() - _vec->front();
	for (size_t i = 0; i < _vec->size() - 1; i++)
	{
		if (static_cast<unsigned int>(_vec->at(i + 1) - _vec->at(i)) < span)
			span = _vec->at(i + 1) - _vec->at(i);
	}
	return (span);
}

/*
 * This method returns the longest span amongst the stored numbers.
 */
unsigned int
Span::longestSpan() const
{
	if (_vec->size() < 2)
		throw NotEnoughNumbersOMGException();

	sort(_vec->begin(), _vec->end());
	return (_vec->back() - _vec->front());
}

/*
 * This method displays the numbers of the Span object to stdout.
 */
void
Span::display() const
{
	cout << "{";
	for (vector<int>::const_iterator it = _vec->begin(); it != _vec->end(); it++)
		cout << " " << *it;
	cout << " }" << endl;
}

/*
 * This method returns the ammount of stored numbers in the Span object.
 */
size_t
Span::size() const
{
	return (_vec->size());
}

/*
 * This method deletes the vector of integers and sets everything to 0.
 */
void
Span::killEmAll()
{
	delete _vec;
	_vec = nullptr;
	_max = 0;
}

/*
 * exception::what() overload for NoMoreRoomSadNoisesException;
 */
const char*
Span::NoMoreRoomSadNoisesException::what() const throw()
{
	return ("The span container is already full :(");
}

/*
 * exception::what() overload for NotEnoughNumbersOMGException.
 */
const char*
Span::NotEnoughNumbersOMGException::what() const throw()
{
	return (" OMG there aren't enough numbers to calculate a span");
}
