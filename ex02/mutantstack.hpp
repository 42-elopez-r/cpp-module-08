/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mutantstack.hpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/23 19:36:15 by elopez-r          #+#    #+#             */
/*   Updated: 2021/07/23 20:56:55 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MUTANTSTACK_HPP
#define MUTANTSTACK_HPP

#include <stack>

using std::stack;

template < typename T >
class MutantStack: public stack<T>
{
	public:
		typedef typename stack<T>::container_type::iterator iterator;
		typedef typename stack<T>::container_type::const_iterator const_iterator;
		typedef typename stack<T>::container_type::reverse_iterator reverse_iterator;
		typedef typename stack<T>::container_type::const_reverse_iterator const_reverse_iterator;

		MutantStack<T>() {}

		MutantStack<T>(const MutantStack<T>& ms): stack<T>(ms) {}

		~MutantStack<T>() {}

		/*
		 * Assignation operator overload.
		 */
		MutantStack&
		operator=(const MutantStack<T>& ms)
		{
			return (stack<T>::operator=(ms));
		}

		/*
		 * This function returns a iterator to the beginning.
		 */
		iterator
		begin()
		{
			return (this->c.begin());
		}

		/*
		 * This function returns a constant iterator to the beginning.
		 */
		const_iterator
		begin() const
		{
			return (this->c.begin());
		}

		/*
		 * This function returns a iterator to the end.
		 */
		iterator
		end()
		{
			return (this->c.end());
		}

		/*
		 * This function returns a constant iterator to the end.
		 */
		const_iterator
		end() const
		{
			return (this->c.end());
		}

		/*
		 * This function returns a reverse iterator to the beginning.
		 */
		iterator
		rbegin()
		{
			return (this->c.rbegin());
		}

		/*
		 * This function returns a constant reverse iterator to the beginning.
		 */
		const_iterator
		rbegin() const
		{
			return (this->c.rbegin());
		}

		/*
		 * This function returns a reverse iterator to the end.
		 */
		iterator
		rend()
		{
			return (this->c.rend());
		}

		/*
		 * This function returns a constant reverse iterator to the end.
		 */
		const_iterator
		rend() const
		{
			return (this->c.rend());
		}
};

#endif
