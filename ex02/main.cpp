/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/23 20:38:33 by elopez-r          #+#    #+#             */
/*   Updated: 2021/07/23 21:16:38 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "mutantstack.hpp"
#include <stack>
#include <iostream>
#include <cstdlib>
#include <ctime>

using std::cout; using std::endl;

template <typename T>
static void
displayMutantStack(MutantStack<T>& ms)
{
	cout << "{";
	for (typename MutantStack<T>::const_iterator it = ms.begin(); it != ms.end(); it++)
		cout << " " << *it;
	cout << " }" << endl;
}

int
main()
{
	MutantStack<int> ms;

	srand(time(NULL));
	for (int i = 0; i < 10; i++)
		ms.push(rand() % 100);
	cout << "MutantStack ms: " << endl;
	displayMutantStack<int>(ms);
	
	cout << endl << "Pop three elements out of ms:" << endl;
	for (int i = 0; i < 3; i++)
		ms.pop();
	displayMutantStack<int>(ms);

	cout << endl << "Change first element to 42 using the iterator:" << endl;
	*ms.begin() = 42;
	displayMutantStack<int>(ms);

	std::stack<int> s(ms);
	cout << endl << "Size of ms: " << ms.size() << endl;
	cout << "Size of s: " << s.size() << endl;
}
